import graphene


class Query(graphene.ObjectType):
    from .types.tag_type import (
        all_tags,
        resolve_all_tags
    )


schema = graphene.Schema(query=Query)
