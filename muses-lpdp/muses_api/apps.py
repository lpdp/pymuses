from django.apps import AppConfig


class MusesApiConfig(AppConfig):
    name = 'muses_api'
