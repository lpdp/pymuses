from django.urls import include, path
from rest_framework import routers
from rest_framework.authtoken import views as rf_views
from . import views


router = routers.DefaultRouter()
router.register(r"members", views.MemberViewSet)
router.register(r"groups", views.GroupViewSet)
router.register(r"admonitions", views.SectionViewSet)
router.register(r"alerts", views.AlertViewSet)
router.register(r"books", views.BookViewSet)
router.register(r"comments", views.CommentViewSet)

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    path("", include(router.urls)),
    path("api-auth/", include("rest_framework.urls", namespace="rest_framework")),
    path("api-token-auth/", rf_views.obtain_auth_token),
]
