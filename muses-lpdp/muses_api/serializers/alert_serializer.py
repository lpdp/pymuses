from rest_framework import serializers

from muses_db.models import Alert


class AlertSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Alert
        fields = [
            "type",
            "details",
            "status",
            "post",
            "comment",
        ]
