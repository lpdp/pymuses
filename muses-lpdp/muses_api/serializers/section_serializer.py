from rest_framework import serializers

from muses_db.models import Section


class SectionSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Section
        fields = ["short_name", "name", "order", "description", "status", "groups"]
