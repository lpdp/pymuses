from rest_framework import serializers

from muses_db.models import Member


class MemberSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Member
        fields = ['url', 'username', 'email', 'groups']