from rest_framework import serializers

from muses_db.models import Book


class BookSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Book
        fields = [
            "title",
            "isbn",
            "summary",
            "published_at",
            "author",
            "license",
            "visible",
        ]
