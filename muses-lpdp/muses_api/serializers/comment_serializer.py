from rest_framework import serializers

from muses_db.models import Comment


class CommentSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Comment
        fields = [
            "author",
            "content",
            "reply_to",
            "readings",
            "ip_address",
        ]
