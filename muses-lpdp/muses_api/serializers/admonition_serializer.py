from rest_framework import serializers

from muses_db.models import Admonition


class AdmonitionSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Admonition
        fields = [
            "subject",
            "content",
            "post",
            "comment",
        ]
