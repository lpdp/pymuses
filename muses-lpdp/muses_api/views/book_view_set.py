from rest_framework import viewsets, permissions

from muses_api.serializers import BookSerializer
from muses_db.models import Book


class BookViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows books to be viewed or edited.
    """

    queryset = Book.objects.all()
    serializer_class = BookSerializer
    permission_classes = [permissions.IsAuthenticated]
