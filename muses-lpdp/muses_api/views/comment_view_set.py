from rest_framework import viewsets, permissions

from muses_api.serializers import CommentSerializer
from muses_db.models import Comment


class CommentViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows comments to be viewed or edited.
    """

    queryset = Comment.objects.all()
    serializer_class = CommentSerializer
    permission_classes = [permissions.IsAuthenticated]
