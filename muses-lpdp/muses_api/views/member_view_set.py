from rest_framework import viewsets, permissions

from muses_api.serializers import MemberSerializer
from muses_db.models import Member


class MemberViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """

    queryset = Member.objects.all().order_by("-date_joined")
    serializer_class = MemberSerializer
    permission_classes = [permissions.IsAuthenticated]
