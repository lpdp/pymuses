from rest_framework import viewsets, permissions

from muses_api.serializers import SectionSerializer
from muses_db.models import Section


class SectionViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows sections to be viewed or edited.
    """

    queryset = Section.objects.all()
    serializer_class = SectionSerializer
    permission_classes = [permissions.IsAuthenticated]
