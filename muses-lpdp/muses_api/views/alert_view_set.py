from rest_framework import viewsets, permissions

from muses_api.serializers import AlertSerializer
from muses_db.models import Alert


class AlertViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """

    queryset = Alert.objects.all()
    serializer_class = AlertSerializer
    permission_classes = [permissions.IsAuthenticated]
