from rest_framework import viewsets, permissions

from muses_api.serializers import AdmonitionSerializer
from muses_db.models import Admonition


class AdmonitionViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """

    queryset = Admonition.objects.all()
    serializer_class = AdmonitionSerializer
    permission_classes = [permissions.IsAuthenticated]
