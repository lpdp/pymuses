import graphene
from graphene_django import DjangoObjectType

from muses_db.models import Tag


class TagType(DjangoObjectType):
    class Meta:
        model = Tag
        fields = (
            "id",
            "name",
            "enable_at",
            "disable_at",
            "mature",
            "active",
            "valid_sections"
        )


all_tags = graphene.List(TagType)


def resolve_all_tags(root, info):
    return Tag.objects.all()
