from .tag_type import TagType

__all__ = [
    "TagType"
]