import logging
from django.conf import settings
from django.core.handlers.wsgi import WSGIRequest

from muses_db.models import SiteParam
from muses_web.forms.set_theme_form import SetThemeForm

logger = logging.getLogger(settings.APPLICATION_NAME)


def global_context_processor(request: WSGIRequest):
    site_params = SiteParam.objects.values()
    values = {}
    for item in site_params:
        values["site_param_" + item.get("key")] = item.get("value")
    logger.debug(f"values={values}")
    values.update({"set_theme_form": SetThemeForm()})
    from ipware import get_client_ip

    client_ip, is_routable = get_client_ip(request)
    if client_ip is None:
        values.update({"ip_address": "Unknown"})
    elif is_routable:
        values.update({"ip_address": client_ip})
    else:
        values.update({"ip_address": f"Private IP: {client_ip}"})
    return values
