import logging
from django.conf import settings
from django.core.handlers.wsgi import WSGIRequest

from muses_db.models import Section

logger = logging.getLogger(settings.APPLICATION_NAME)


def main_menu_context_processor(request: WSGIRequest):
    sections = Section.objects.actives()
    logger.debug(f"sections={sections}")
    return {"sections": sections}
