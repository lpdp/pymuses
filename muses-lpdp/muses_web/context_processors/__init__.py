from .global_context_processor import global_context_processor
from .main_menu_context_processor import main_menu_context_processor

__all__ = [
    "global_context_processor",
    "main_menu_context_processor",
]