from .generic import TemplateView


class IndexView(TemplateView):
    template_name = "web/index.html"
