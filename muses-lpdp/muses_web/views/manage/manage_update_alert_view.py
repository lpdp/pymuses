from django.contrib.auth.decorators import login_required
from django.urls import reverse
from django.utils.decorators import method_decorator

from muses_db.models import Alert
from muses_web.views.generic import UpdateView


@method_decorator(login_required, name="dispatch")
class ManageUpdateAlertView(UpdateView):
    template_name = "web/manage/update_alert.html"
    model = Alert
    pk_url_kwarg = "id"

    def get_success_url(self):
        return reverse("manage_alert_detail", kwargs={"id": self.id})
