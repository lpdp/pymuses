from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator

from muses_db.models import Alert
from muses_web.views.generic import DetailView


@method_decorator(login_required, name="dispatch")
class ManageDetailAlertView(DetailView):
    template_name = "web/manage/detail_alert.html"
    model = Alert
    pk_url_kwarg = "id"
