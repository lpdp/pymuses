from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator

from muses_db.models import License
from muses_web.views.generic import DetailView


@method_decorator(login_required, name="dispatch")
class ManageDetailLicenseView(DetailView):
    template_name = "web/manage/detail_license.html"
    model = License
    slug_field = "name"
    slug_url_kwarg = "name"
    pk_url_kwarg = "id"
