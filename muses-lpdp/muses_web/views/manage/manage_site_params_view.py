from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator

from muses_db.models import SiteParam
from muses_web.views.generic import ListView


@method_decorator(login_required, name="dispatch")
class ManageSiteParamsView(ListView):
    template_name = "web/manage/site_params.html"
    model = SiteParam
