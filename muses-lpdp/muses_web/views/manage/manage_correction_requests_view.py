from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator

from muses_db.models import CorrectionRequest
from muses_web.views.generic import ListView


@method_decorator(login_required, name="dispatch")
class ManageCorrectionRequestsView(ListView):
    template_name = "web/manage/correction_requests.html"
    model = CorrectionRequest
    paginate_by = 30
