from django.contrib.auth.decorators import login_required
from django.utils import timezone
from django.utils.decorators import method_decorator

from muses_db.models import Tag
from muses_web.views.generic import ListView


@method_decorator(login_required, name="dispatch")
class ManageTagsView(ListView):
    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(object_list=object_list, **kwargs)
        context["now"] = timezone.now()
        return context

    template_name = "web/manage/tags.html"
    model = Tag
    paginate_by = 30
