from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator

from muses_db.models import SiteParam
from muses_web.views.generic import CreateView


@method_decorator(login_required, name="dispatch")
class ManageCreateSiteParamView(CreateView):
    template_name = "web/manage/create_site_param.html"
    model = SiteParam
    fields = [
        "key",
        "value",
    ]
