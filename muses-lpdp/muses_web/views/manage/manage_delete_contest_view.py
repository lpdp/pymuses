from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator

from muses_db.models import Contest
from muses_web.views.generic import DeleteView


@method_decorator(login_required, name="dispatch")
class ManageDeleteContestView(DeleteView):
    template_name = "web/manage/delete_contest.html"
    model = Contest
