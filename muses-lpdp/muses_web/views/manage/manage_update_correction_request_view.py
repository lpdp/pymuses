from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator

from muses_db.models import CorrectionRequest
from muses_web.views.generic import UpdateView


@method_decorator(login_required, name="dispatch")
class ManageUpdateCorrectionRequestView(UpdateView):
    template_name = "web/manage/update_correction_request.html"
    model = CorrectionRequest
    slug_field = "id"
    slug_url_kwarg = "id"
