from django.contrib.auth.decorators import login_required
from django.urls import reverse
from django.utils.decorators import method_decorator

from muses_db.models import Contest
from muses_web.views.generic import DetailView


@method_decorator(login_required, name="dispatch")
class ManageDetailContestView(DetailView):
    template_name = "web/manage/detail_contest.html"
    model = Contest
    slug_field = "name"
    slug_url_kwarg = "name"
    pk_url_kwarg = "id"
