from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator

from muses_db.models import Alert
from muses_web.views.generic import CreateView


@method_decorator(login_required, name="dispatch")
class ManageCreateAlertView(CreateView):
    template_name = "web/manage/create_alert.html"
    model = Alert
    fields = ["type", "details", "status", "post", "comment"]
