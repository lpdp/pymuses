from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator

from muses_db.models import CorrectionRequest
from muses_web.views.generic import DeleteView


@method_decorator(login_required, name="dispatch")
class ManageDeleteCorrectionRequestView(DeleteView):
    template_name = "web/manage/delete_correction_request.html"
    model = CorrectionRequest
