from .manage_admonitions_view import *
from .manage_detail_admonition_view import *
from .manage_create_admonition_view import *
from .manage_update_admonition_view import *
from .manage_delete_admonition_view import *
from .manage_alerts_view import *
from .manage_create_alert_view import *
from .manage_detail_alert_view import *
from .manage_update_alert_view import *
from .manage_delete_alert_view import *
from .manage_contests_view import *
from .manage_detail_contest_view import *
from .manage_delete_contest_view import *
from .manage_licenses_view import *
from .manage_detail_license_view import *
from .manage_create_license_view import *
from .manage_update_license_view import *
from .manage_delete_license_view import *
from .manage_site_params_view import *
from .manage_detail_site_param_view import *
from .manage_create_site_param_view import *
from .manage_update_site_param_view import *
from .manage_delete_site_param_view import *
from .manage_tags_view import *
from .manage_detail_tag_view import *
from .manage_create_contest_view import *
from .manage_update_contest_view import *
from .manage_create_tag_view import *
from .manage_update_tag_view import *
from .manage_delete_tag_view import *
from .manage_correction_requests_view import *
from .manage_detail_correction_request_view import *
from .manage_update_correction_request_view import *
from .manage_delete_correction_request_view import *

__all__ = [
    "ManageAdmonitionsView",
    "ManageDetailAdmonitionView",
    "ManageCreateAdmonitionView",
    "ManageUpdateAdmonitionView",
    "ManageAlertsView",
    "ManageDetailAlertView",
    "ManageCreateAlertView",
    "ManageUpdateAlertView",
    "ManageDeleteAlertView",
    "ManageContestsView",
    "ManageDetailContestView",
    "ManageDeleteContestView",
    "ManageLicensesView",
    "ManageDetailLicenseView",
    "ManageCreateLicenseView",
    "ManageUpdateLicenseView",
    "ManageDeleteLicenseView",
    "ManageSiteParamsView",
    "ManageDetailSiteParamView",
    "ManageCreateSiteParamView",
    "ManageUpdateSiteParamView",
    "ManageDeleteSiteParamView",
    "ManageDeleteAdmonitionView",
    "ManageTagsView",
    "ManageDetailTagView",
    "ManageCreateContestView",
    "ManageUpdateContestView",
    "ManageCreateTagView",
    "ManageUpdateTagView",
    "ManageDeleteTagView",
    "ManageCorrectionRequestsView",
    "ManageDetailCorrectionRequestView",
    "ManageUpdateCorrectionRequestView",
    "ManageDeleteCorrectionRequestView"
]