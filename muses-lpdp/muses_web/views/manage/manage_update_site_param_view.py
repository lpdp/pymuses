from django.contrib.auth.decorators import login_required
from django.urls import reverse
from django.utils.decorators import method_decorator

from muses_db.models import SiteParam
from muses_web.views.generic import UpdateView


@method_decorator(login_required, name="dispatch")
class ManageUpdateSiteParamView(UpdateView):
    template_name = "web/manage/update_site_param.html"
    model = SiteParam
    fields = ["key", "value"]
    slug_field = "key"
    slug_url_kwarg = "key"
    pk_url_kwarg = "id"

    def get_success_url(self):
        return reverse("manage_site_params_detail", kwargs={"id": self.id})
