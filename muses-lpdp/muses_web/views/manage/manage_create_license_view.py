from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator

from muses_db.models import License
from muses_web.views.generic import CreateView


@method_decorator(login_required, name="dispatch")
class ManageCreateLicenseView(CreateView):
    template_name = "web/manage/create_license.html"
    model = License
    fields = ["name", "text", "logo", "active"]
