from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator

from muses_db.models.alert import Alert
from muses_web.views.generic import ListView


@method_decorator(login_required, name="dispatch")
class ManageAlertsView(ListView):
    template_name = "web/manage/alerts.html"
    model = Alert
