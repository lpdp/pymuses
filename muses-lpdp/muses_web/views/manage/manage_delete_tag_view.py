from django.contrib.auth.decorators import login_required
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator

from muses_db.models import Tag
from muses_web.views.generic import DeleteView


@method_decorator(login_required, name="dispatch")
class ManageDeleteTagView(DeleteView):
    template_name = "web/manage/delete_tag.html"
    model = Tag
    slug_field = "name"
    slug_url_kwarg = "name"
    pk_url_kwarg = "id"
    success_url = reverse_lazy("manage_tags")
