from django.contrib.auth.decorators import login_required
from django.urls import reverse
from django.utils.decorators import method_decorator

from muses_db.models import License
from muses_web.views.generic import UpdateView


@method_decorator(login_required, name="dispatch")
class ManageUpdateLicenseView(UpdateView):
    template_name = "web/manage/update_license.html"
    model = License
    fields = ["name", "text", "logo", "active"]
    slug_field = "name"
    slug_url_kwarg = "name"
    pk_url_kwarg = "id"

    def get_success_url(self):
        return reverse("manage_licenses_detail", kwargs={"id": self.id})
