from django.contrib.auth.decorators import login_required
from django.urls import reverse
from django.utils.decorators import method_decorator

from muses_db.models import Tag
from muses_web.views.generic import UpdateView


@method_decorator(login_required, name="dispatch")
class ManageUpdateTagView(UpdateView):
    template_name = "web/manage/update_tag.html"
    model = Tag
    slug_field = "name"
    slug_url_kwarg = "name"
    pk_url_kwarg = "id"

    def get_success_url(self):
        return reverse("manage_tag_detail", kwargs={"id": self.id})
