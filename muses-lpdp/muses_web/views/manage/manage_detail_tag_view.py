from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator

from muses_db.models import Tag
from muses_web.views.generic import DetailView


@method_decorator(login_required, name="dispatch")
class ManageDetailTagView(DetailView):
    template_name = "web/manage/detail_tag.html"
    model = Tag
    slug_field = "name"
    slug_url_kwarg = "name"
    pk_url_kwarg = "id"
