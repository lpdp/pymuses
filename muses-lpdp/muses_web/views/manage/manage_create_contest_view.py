from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator

from muses_db.models import Contest
from muses_web.views.generic import CreateView


@method_decorator(login_required, name="dispatch")
class ManageCreateContestView(CreateView):
    template_name = "web/manage/create_contest.html"
    model = Contest
    fields = ["name", "starts_at", "ends_at"]
