from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator

from muses_db.models import Contest
from muses_web.views.generic import UpdateView


@method_decorator(login_required, name="dispatch")
class ManageUpdateContestView(UpdateView):
    template_name = "web/manage/update_contest.html"
    model = Contest
    slug_field = "id"
    slug_url_kwarg = "id"
