from django.contrib.auth.decorators import login_required
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator

from muses_db.models import SiteParam
from muses_web.views.generic import DeleteView


@method_decorator(login_required, name="dispatch")
class ManageDeleteSiteParamView(DeleteView):
    template_name = "web/manage/delete_site_param.html"
    model = SiteParam
    slug_field = "key"
    slug_url_kwarg = "key"
    pk_url_kwarg = "id"
    success_url = reverse_lazy("manage_site_params")
