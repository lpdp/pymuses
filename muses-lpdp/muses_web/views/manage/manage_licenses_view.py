from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator

from muses_db.models import License
from muses_web.views.generic import ListView


@method_decorator(login_required, name="dispatch")
class ManageLicensesView(ListView):
    # View to manage license types for posts
    template_name = "web/manage/licenses.html"
    model = License
