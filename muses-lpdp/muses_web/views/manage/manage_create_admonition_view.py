from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator

from muses_db.models import Admonition
from muses_web.views.generic import CreateView


@method_decorator(login_required, name="dispatch")
class ManageCreateAdmonitionView(CreateView):
    template_name = "web/manage/create_admonition.html"
    model = Admonition
    fields = ["subject", "content", "post", "comment"]
