from django.contrib.auth.decorators import login_required
from django.urls import reverse
from django.utils.decorators import method_decorator

from muses_db.models import CorrectionRequest
from muses_web.views.generic import DetailView


@method_decorator(login_required, name="dispatch")
class ManageDetailCorrectionRequestView(DetailView):
    template_name = "web/manage/detail_correction_request.html"
    model = CorrectionRequest
    slug_field = "id"
    slug_url_kwarg = "id"

    def get_success_url(self):
        return reverse("myUrl", kwargs={"pk": self.pk})
