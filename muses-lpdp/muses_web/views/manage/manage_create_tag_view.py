from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator

from muses_db.models import Tag
from muses_web.views.generic import CreateView


@method_decorator(login_required, name="dispatch")
class ManageCreateTagView(CreateView):
    template_name = "web/manage/create_tag.html"
    model = Tag
    fields = ["name", "starts_at", "ends_at"]
