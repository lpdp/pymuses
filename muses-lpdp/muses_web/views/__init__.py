from .index_view import IndexView
from .login_view import LoginView
from .view import View
from .list_faq_view import ListFaqView
from .list_members_view import ListMembersView
from .list_posts_view import ListPostsView
from .view_book_view import ViewBookView
from .view_post_view import ViewPostView
from .view_post_new import ViewPostNew
from .upload_my_avatar import upload_my_avatar
from .forget_password_view import ForgotPasswordView
from .register_view import RegisterView

__all__ = [
    "View",
    "IndexView",
    "LoginView",
    "ListFaqView",
    "ListMembersView",
    "ListPostsView",
    "ViewBookView",
    "ViewPostView",
    "ViewPostNew",
    "ForgotPasswordView",
    "RegisterView",
]
