from .generic import TemplateView


class RegisterView(TemplateView):
    template_name = "web/register.html"
