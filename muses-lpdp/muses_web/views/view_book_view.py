from .generic import TemplateView


class ViewBookView(TemplateView):
    template_name = "web/view_book.html"
