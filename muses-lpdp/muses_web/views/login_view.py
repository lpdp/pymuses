from .generic.form_view import FormView
from ..forms import LoginForm


class LoginView(FormView):
    template_name = "web/login.html"
    form_class = LoginForm
    success_url = "."
