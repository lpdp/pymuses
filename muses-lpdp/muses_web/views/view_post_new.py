from .generic import TemplateView


class ViewPostNew(TemplateView):
    template_name = "web/new_post.html"