from .generic import TemplateView


class ViewContestView(TemplateView):
    template_name = "web/view_contest.html"
