
from .generic import FormView
from ..forms.forgot_password_form import ForgotPasswordForm


class ForgotPasswordView(FormView):
    template_name = "web/forget_password.html"
    form_class = ForgotPasswordForm
    success_url = "."
