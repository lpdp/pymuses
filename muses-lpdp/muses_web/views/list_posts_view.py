from muses_db.models import Post, Section
from .generic import ListView


class ListPostsView(ListView):
    template_name = "web/posts.html"
    model = Post

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        section_short_name = self.kwargs.get('section_short_name')
        context.update({
            'section_short_name': section_short_name,
            'section_name': Section.objects.get(short_name= section_short_name).name
        })

        return context
