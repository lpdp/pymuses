from .generic import TemplateView


class ViewPostView(TemplateView):
    template_name = "web/view_post.html"
