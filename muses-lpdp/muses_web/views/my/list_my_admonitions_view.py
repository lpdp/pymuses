from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator

from muses_db.models import Admonition
from muses_web.views.generic import ListView


@method_decorator(login_required, name="dispatch")
class ListMyAdmonitionsView(ListView):
    template_name = "web/my/my_admonitions.html"
    model = Admonition
