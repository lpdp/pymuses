from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator

from muses_web.forms import MyPreferencesForm
from muses_web.views.generic import FormView


@method_decorator(login_required, name="dispatch")
class ListMyPreferencesView(FormView):
    template_name = "web/my/my_preferences.html"
    form_class = MyPreferencesForm
