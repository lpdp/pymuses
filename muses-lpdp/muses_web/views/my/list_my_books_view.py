from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator

from muses_db.models import Book
from muses_web.views.generic import ListView


@method_decorator(login_required, name="dispatch")
class ListMyBooksView(ListView):
    template_name = "web/my/my_books.html"
    model = Book
