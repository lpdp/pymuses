from .list_view import ListView
from .template_view import TemplateView
from .form_view import FormView
from .create_view import CreateView
from .update_view import UpdateView
from .delete_view import DeleteView
from .detail_view import DetailView

__all__ = [
    "ListView",
    "TemplateView",
    "FormView",
    "CreateView",
    "UpdateView",
    "DeleteView",
    "DetailView"
]
