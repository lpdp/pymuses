from .generic import TemplateView


class ViewMemberView(TemplateView):
    template_name = "web/view_member.html"
