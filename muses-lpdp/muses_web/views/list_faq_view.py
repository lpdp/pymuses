from muses_db.models import Faq
from .generic import ListView


class ListFaqView(ListView):
    template_name = "web/faq.html"
    model = Faq
