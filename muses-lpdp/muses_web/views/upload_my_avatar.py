import tempfile
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt

from muses_web.forms import UploadImageFileForm


def handle_uploaded_file(f):
    destination = tempfile.TemporaryFile()
    with destination:
        for chunk in f.chunks():
            destination.write(chunk)


@csrf_exempt
def upload_my_avatar(request):
    if request.method == 'POST':
        form = UploadImageFileForm(request.POST, request.FILES)
        if form.is_valid():
            handle_uploaded_file(request.FILES['file'])
            return HttpResponseRedirect('/success/url/')
    else:
        form = UploadImageFileForm()
    return render(request, 'web/upload_my_avatar.html', {'form': form})
