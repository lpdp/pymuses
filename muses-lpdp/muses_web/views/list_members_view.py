from muses_db.models import Member
from .generic import ListView


class ListMembersView(ListView):
    template_name = "web/members.html"
    model = Member

    def get_queryset(self):
        return self.model.objects.all()

