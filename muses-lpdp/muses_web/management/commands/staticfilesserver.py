from argparse import ArgumentParser
from django.core.management import BaseCommand
from django.core.management.base import no_translations
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from fastapi.staticfiles import StaticFiles
import uvicorn
import uuid
import os


class Command(BaseCommand):
    help = 'Run static files server'

    def __init__(self):
        super().__init__()
        self.app = FastAPI()
        origins = ["*"]
        self.app.add_middleware(
            CORSMiddleware,
            allow_origins=origins,
            allow_credentials=True,
            allow_methods=["*"],
            allow_headers=["*"],
        )
        self.uuid = uuid.uuid4().hex

    def add_arguments(self, parser: ArgumentParser):
        parser.add_argument("-w", "--web_folder", type=str, default="/", nargs=1)
        parser.add_argument("-f", "--folder", type=str, default=".", nargs=1)
        parser.add_argument("-n", "--name", type=str, default=self.uuid, nargs=1)
        parser.add_argument("-p", "--port", type=int, default=8001, nargs=1)

    @no_translations
    def handle(self, *args, **options):
        self.stdout.write("Starting static files server...")
        test_path = os.path.join(options["folder"][0])
        real_path = os.path.realpath(test_path)
        self.app.mount(options["web_folder"][0], StaticFiles(directory=real_path), name=options["name"])
        uvicorn.run(self.app, port=options["port"][0])
