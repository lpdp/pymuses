from django.http import HttpRequest, HttpResponse

from muses_db.models import Member


class Theme:
    def __init__(self, get_response):
        self.get_response = get_response
        # One-time configuration and initialization.

    def __call__(self, request: HttpRequest):
        # Code to be executed for each request before
        # the view (and later middleware) are called.
        theme = request.COOKIES.get("theme", None)
        if theme is None:
            theme = Member.THEME_DEFAULT
            request.COOKIES.update({"theme": theme})

        response = self.get_response(request)

        response.set_cookie("theme", theme)

        # Code to be executed for each request/response after
        # the view is called.

        return response
