import django.forms


class Form(django.forms.Form):
    def as_div(self):
        """Return this form rendered as HTML <div>s -- excluding the <div></div>."""
        return self._html_output(
            normal_row='<div%(html_class_attr)s><div>%(label)s</div><div>%(errors)s%(field)s%(help_text)s</div></div>',
            error_row='<div class="ms-error-row"><span class="ms-error">%s</span></div>',
            row_ender='</div></div>',
            help_text_html='<div class="ms-helptext-row"><span class="ms-helptext">%s</span></div>',
            errors_on_separate_row=True,
        )


