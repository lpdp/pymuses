from muses_db.models.admonition_template import AdmonitionTemplate
from muses_web.forms.core import ModelForm


class AdmonitionTemplateForm(ModelForm):
    class Meta:
        model = AdmonitionTemplate
        fields = [
            "subject",
            "content"
        ]