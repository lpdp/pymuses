from muses_db.models import License
from muses_web.forms.core import ModelForm


class LicenseForm(ModelForm):
    class Meta:
        model = License
        fields = [
            "name",
            "text",
            "logo",
            "active"
        ]
