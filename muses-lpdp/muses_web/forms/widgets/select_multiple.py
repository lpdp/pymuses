import django.forms.widgets


class SelectMultiple(django.forms.widgets.SelectMultiple):
    template_name = "web/forms/widgets/select.html"
    option_template_name = "web/forms/widgets/select_option.html"
