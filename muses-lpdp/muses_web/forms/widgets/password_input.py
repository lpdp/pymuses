import django.forms.widgets


class PasswordInput(django.forms.widgets.PasswordInput):
    css_classes = "ms-password-input"
    template_name = "web/forms/widgets/password.html"

    def __init__(self, attrs=None, render_value=False):
        if attrs is None or attrs.get('class') is None:
            attrs = {}
            attrs.update({'class': f"{self.css_classes}"})
        else:
            attrs.update({'class': attrs.get('class') + f" {self.css_classes}"})
        super().__init__(attrs, render_value)