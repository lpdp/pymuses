import django.forms.widgets


class ClearableFileInput(django.forms.widgets.ClearableFileInput):
    template_name = "web/forms/widgets/clearable_file_input.html"
