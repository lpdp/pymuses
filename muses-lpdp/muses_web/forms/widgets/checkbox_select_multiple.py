import django.forms.widgets


class CheckboxSelectMultiple(django.forms.widgets.CheckboxSelectMultiple):
    css_classes = "ms-checkbox-select-multiple-input"
    template_name = "web/forms/widgets/checkbox_select.html"
    option_template_name = "web/forms/widgets/checkbox_option.html"

    def __init__(self, attrs=None, choices=()):
        if attrs is None or attrs.get('class') is None:
            attrs = {}
            attrs.update({'class': f"{self.css_classes}"})
        else:
            attrs.update({'class': attrs.get('class') + f" {self.css_classes}"})
        super().__init__(attrs, choices)



