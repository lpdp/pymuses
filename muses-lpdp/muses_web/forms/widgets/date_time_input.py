import django.forms.widgets


class DateTimeInput(django.forms.widgets.DateTimeInput):
    css_classes = "ms-date-time-input"
    template_name = "web/forms/widgets/datetime.html"

    def __init__(self, attrs=None, format=None):
        if attrs is None or attrs.get('class') is None:
            attrs = {}
            attrs.update({'class': f"{self.css_classes}"})
        else:
            attrs.update({'class': attrs.get('class') + f" {self.css_classes}"})
        super().__init__(attrs, format)
