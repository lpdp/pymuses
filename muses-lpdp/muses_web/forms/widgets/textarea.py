import django.forms.widgets


class Textarea(django.forms.widgets.Textarea):
    template_name = "web/forms/widgets/textarea.html"
