import django.forms.widgets


class HiddenInput(django.forms.widgets.HiddenInput):
    template_name = "web/forms/widgets/hidden.html"
