import django.forms.widgets


class MultipleHiddenInput(django.forms.widgets.MultipleHiddenInput):
    template_name = "web/forms/widgets/multiple_hidden.html"
