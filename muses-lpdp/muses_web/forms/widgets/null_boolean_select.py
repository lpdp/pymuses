import django.forms.widgets


class NullBooleanSelect(django.forms.widgets.NullBooleanSelect):
    template_name = "web/forms/widgets/select.html"
    option_template_name = "web/forms/widgets/select_option.html"
