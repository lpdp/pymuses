import django.forms.widgets


class CheckboxInput(django.forms.widgets.CheckboxInput):
    css_classes = "ms-text-input"
    template_name = "web/forms/widgets/checkbox.html"

    def __init__(self, attrs=None, check_test=None):
        if attrs is None or attrs.get('class') is None:
            attrs = {}
            attrs.update({'class': f"{self.css_classes}"})
        else:
            attrs.update({'class': attrs.get('class') + f" {self.css_classes}"})
        super().__init__(attrs, check_test)


