import django.forms.widgets


class RadioSelect(django.forms.widgets.RadioSelect):
    template_name = "web/forms/widgets/radio.html"
    option_template_name = "web/forms/widgets/radio_option.html"
