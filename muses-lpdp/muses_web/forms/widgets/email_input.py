import django.forms.widgets


class EmailInput(django.forms.widgets.EmailInput):
    template_name = "web/forms/widgets/email.html"
