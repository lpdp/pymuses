import django.forms.widgets


class URLInput(django.forms.widgets.URLInput):
    template_name = "web/forms/widgets/url.html"
