import django.forms.widgets


class SelectDateWidget(django.forms.widgets.SelectDateWidget):
    template_name = "web/forms/widgets/select_date.html"
