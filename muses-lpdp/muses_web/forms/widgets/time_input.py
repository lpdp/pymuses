import django.forms.widgets


class TimeInput(django.forms.widgets.TimeInput):
    template_name = "web/forms/widgets/time.html"
