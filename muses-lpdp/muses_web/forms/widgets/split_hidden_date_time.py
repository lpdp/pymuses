import django.forms.widgets


class SplitHiddenDateTimeWidget(django.forms.widgets.SplitHiddenDateTimeWidget):
    template_name = "web/forms/widgets/splithiddendatetime.html"
