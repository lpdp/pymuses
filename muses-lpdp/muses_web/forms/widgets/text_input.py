import django.forms.widgets


class TextInput(django.forms.widgets.TextInput):
    css_classes = "ms-text-input"
    template_name = "web/forms/widgets/text.html"

    def __init__(self, attrs=None):
        if attrs is None or attrs.get('class') is None:
            attrs = {}
            attrs.update({'class': f"{self.css_classes}"})
        else:
            attrs.update({'class': attrs.get('class') + f" {self.css_classes}"})
        super().__init__(attrs)


