import django.forms.widgets


class NumberInput(django.forms.widgets.NumberInput):
    template_name = "web/forms/widgets/number.html"
