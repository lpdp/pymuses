import django.forms.widgets


class FileInput(django.forms.widgets.FileInput):
    template_name = "web/forms/widgets/file.html"
