import django.forms.widgets


class SplitDateTimeWidget(django.forms.widgets.SplitDateTimeWidget):
    template_name = "web/forms/widgets/splitdatetime.html"
