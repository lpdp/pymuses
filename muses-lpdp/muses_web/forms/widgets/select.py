import django.forms.widgets


class Select(django.forms.widgets.Select):
    template_name = "web/forms/widgets/select.html"
