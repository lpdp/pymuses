from django.utils.translation import gettext_lazy as _

from . import ImageField
from .core import Form


class UploadImageFileForm(Form):
    image = ImageField(
        label=_("Select your image")
    )
