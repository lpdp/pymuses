from .core import Form

from django.utils.translation import gettext_lazy as _

from .widgets import PasswordInput, TextInput
from .. import forms


class LoginForm(Form):
    login = forms.CharField(
        label=_("login"),
        max_length=100,
        widget=TextInput()
    )
    password = forms.CharField(
        label=_("password"),
        widget=PasswordInput()
    )
