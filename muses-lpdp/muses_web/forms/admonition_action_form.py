from muses_db.models import AdmonitionAction
from muses_web.forms.core import ModelForm


class AdmonitionActionForm(ModelForm):
    class Meta:
        model = AdmonitionAction
        fields = [
            "message",
            "type"
        ]