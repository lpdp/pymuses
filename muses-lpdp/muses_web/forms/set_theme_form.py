from muses_db.models import Member
from . import ChoiceField
from .core import Form
from django.utils.translation import gettext_lazy as _


class SetThemeForm(Form):
    theme = ChoiceField(
        label=_("Theme"),
        choices=Member.THEME_CHOICES,
    )
