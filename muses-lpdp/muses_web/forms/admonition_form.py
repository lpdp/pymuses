from muses_db.models import Admonition
from muses_web.forms.core import ModelForm


class AdmonitionForm(ModelForm):
    class Meta:
        model = Admonition
        fields = [
            "subject",
            "comment"
        ]