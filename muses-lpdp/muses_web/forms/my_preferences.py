from muses_db.models import Member
from . import BooleanField, ChoiceField
from .core import Form
from django.utils.translation import gettext_lazy as _


class MyPreferencesForm(Form):
    theme = ChoiceField(
        label=_("Theme"),
        choices=Member.THEME_CHOICES,

    )
    mailbox_collapsing = BooleanField(label=_("Mailbox collapsing"))
    email_notification_on_new_message = BooleanField(label=_("Email notification on new message"))
    email_notification_on_new_comment = BooleanField(label=_("Email notification on new comment"))

