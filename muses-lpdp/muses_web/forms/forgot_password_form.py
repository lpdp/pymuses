from .core import Form
from .widgets import EmailInput
from .. import forms
from django.utils.translation import gettext_lazy as _


class ForgotPasswordForm(Form):
    login = forms.CharField(
        label=_("Login name"),
        max_length=100
    )
    email = forms.CharField(
        label=_("Email"),
        widget=EmailInput()
    )
