from django.urls import path, include
from . import views
from .views.generic import TemplateView
from .views.manage import ManageAdmonitionsView, ManageAlertsView, ManageContestsView, ManageTagsView, \
    ManageSiteParamsView, ManageLicensesView, ManageCreateContestView, ManageCreateTagView, \
    ManageUpdateAdmonitionView, ManageDetailAdmonitionView, ManageDeleteAdmonitionView, ManageCreateSiteParamView, \
    ManageDetailSiteParamView, ManageUpdateSiteParamView, ManageDeleteSiteParamView, ManageCreateAdmonitionView, \
    ManageDetailTagView, ManageDetailAlertView, ManageCreateAlertView, ManageUpdateAlertView, ManageDeleteAlertView, \
    ManageUpdateContestView, ManageDeleteContestView, ManageDetailContestView, ManageDetailLicenseView, \
    ManageCreateLicenseView, ManageUpdateLicenseView, ManageDeleteLicenseView, ManageUpdateTagView, ManageDeleteTagView, \
    ManageCorrectionRequestsView, ManageDetailCorrectionRequestView, ManageUpdateCorrectionRequestView, \
    ManageDeleteCorrectionRequestView
from .views.my import ListMyAdmonitionsView, ListMyBooksView, ListMyMessagesView, ListMyPreferencesView

manage_urlpatterns = [

    # Admonitions management
    path("admonitions", ManageAdmonitionsView.as_view(), name="manage_admonitions"),
    path("admonitions/<int:id>", ManageDetailAdmonitionView.as_view(), name="manage_admonitions_detail"),
    path("admonitions/new", ManageCreateAdmonitionView.as_view(), name="manage_admonitions_create"),
    path("admonitions/<int:id>/edit", ManageUpdateAdmonitionView.as_view(), name="manage_admonitions_update"),
    path("admonitions/<int:id>/delete", ManageDeleteAdmonitionView.as_view(), name="manage_admonitions_delete"),

    # Alerts management
    path("alerts", ManageAlertsView.as_view(), name="manage_alerts"),
    path("alerts/<int:id>", ManageDetailAlertView.as_view(), name="manage_alerts_detail"),
    path("alerts/new", ManageCreateAlertView.as_view(), name="manage_alerts_create"),
    path("alerts/<int:id>/edit", ManageUpdateAlertView.as_view(), name="manage_alerts_update"),
    path("alerts/<int:id>/delete", ManageDeleteAlertView.as_view(), name="manage_alerts_delete"),

    # Contest management
    path("contests", ManageContestsView.as_view(), name="manage_contests"),
    path("contests/<int:id>", ManageDetailContestView.as_view(), name="manage_contests_detail"),
    path("contests/new", ManageCreateContestView.as_view(), name="manage_contests_create"),
    path("contests/<int:id>/edit", ManageUpdateContestView.as_view(), name="manage_contests_update"),
    path("contests/<int:id>/delete", ManageDeleteContestView.as_view(), name="manage_contests_delete"),
    # path("contests/<int:id>/judging_panel", ),

    # Correction requests management
    path("correction_requests", ManageCorrectionRequestsView.as_view(), name="manage_correction_requests"),
    path("correction_requests/<int:id>", ManageDetailCorrectionRequestView.as_view(),
         name="manage_correction_requests_detail"),
    path("correction_requests/<int:id>/edit", ManageUpdateCorrectionRequestView.as_view(),
         name="manage_correction_requests_update"),
    path("correction_requests/<int:id>/delete", ManageDeleteCorrectionRequestView.as_view(),
         name="manage_correction_requests_delete"),

    # Licenses management
    path("licenses", ManageLicensesView.as_view(), name="manage_licenses"),
    path("licenses/<int:id>", ManageDetailLicenseView.as_view(), name="manage_licenses_detail"),
    path("licenses/new", ManageCreateLicenseView.as_view(), name="manage_licenses_create"),
    path("licenses/<int:id>/edit", ManageUpdateLicenseView.as_view(), name="manage_licenses_update"),
    path("licenses/<int:id>/delete", ManageDeleteLicenseView.as_view(), name="manage_licenses_delete"),

    # Site parameters management
    path("site_params", ManageSiteParamsView.as_view(), name="manage_site_params"),
    path("site_params/<int:id>", ManageDetailSiteParamView.as_view(), name="manage_site_params_detail"),
    path("site_params/new", ManageCreateSiteParamView.as_view(), name="manage_site_params_create"),
    path("site_params/<int:id>/edit", ManageUpdateSiteParamView.as_view(), name="manage_site_params_update"),
    path("site_params/<int:id>/delete", ManageDeleteSiteParamView.as_view(), name="manage_site_params_delete"),

    # Tags management
    path("tags", ManageTagsView.as_view(), name="manage_tags"),
    path("tags/<int:id>", ManageDetailTagView.as_view(), name="manage_tags_detail"),
    path("tags/new", ManageCreateTagView.as_view(), name="manage_tags_create"),
    path("tags/<int:id>/edit", ManageUpdateTagView.as_view(), name="manage_tags_update"),
    path("tags/<int:id>/delete", ManageDeleteTagView.as_view(), name="manage_tags_delete"),

]

correction_urlpatterns = [
]

my_urlpatterns = [
    path("admonitions", ListMyAdmonitionsView.as_view(), name="my_admonitions"),
    path("books", ListMyBooksView.as_view(), name="my_books"),
    path("messages", ListMyMessagesView.as_view(), name="my_messages"),
    path("preferences", ListMyPreferencesView.as_view(), name="my_preferences"),
    path("uploadmyavatar", views.upload_my_avatar, name="my_upload_avatar")
]

urlpatterns = [
    path("home/", views.IndexView.as_view(), name="home"),
    path("login/", views.LoginView.as_view(), name="login"),
    path("faq/", views.ListFaqView.as_view(), name="faq"),
    path("help/", TemplateView.as_view(template_name="web/help.html"), name="help"),
    path("members/", views.ListMembersView.as_view(), name="members"),
    path("posts/<str:section_short_name>", views.ListPostsView.as_view(), name="posts"),
    path("posts/new", views.ViewPostNew.as_view(), name="post_new"),
    path("manage/", include(manage_urlpatterns)),
    path("my/", include(my_urlpatterns)),
    path("forgotyourpassword/", views.ForgotPasswordView.as_view(), name="forgot_password"),
    path("register/", views.RegisterView.as_view(), name="register")
]
