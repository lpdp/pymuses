from django.apps import AppConfig


class MusesWebConfig(AppConfig):
    name = 'muses_web'
