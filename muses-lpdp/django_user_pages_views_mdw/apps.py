from django.apps import AppConfig


class DjangoUserPagesViewsMdwConfig(AppConfig):
    name = 'django_user_pages_views_mdw'
