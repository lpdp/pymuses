import hiyapyco

from .exception import ConfigBoxCredentialsRootNotFound, ConfigBoxConfigurationRootNotFound


class ConfigBox(object):
    def __init__(self, *args, **kwargs):
        self.paths = []
        for path in args:
            self.paths.append(path)
        self.config = hiyapyco.load(*args, **kwargs)

    def __call__(self):
        return self.config.get("configuration", None)

    def get(self, name: str):
        configuration = self.config.get("configuration", None)
        if configuration is None:
            raise ConfigBoxConfigurationRootNotFound()
        try:
            config_item = configuration[name]
        except KeyError:
            config_item = None
        return config_item

    def get_credential(self, name: str):
        credentials = self.config.get("credentials", None)
        if credentials is None:
            raise ConfigBoxCredentialsRootNotFound()
        try:
            credential = credentials[name]
        except KeyError:
            credential = None
        return credential
