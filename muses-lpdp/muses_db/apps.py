from django.db.models.signals import post_migrate
from django.apps import AppConfig


def create_required_objects(sender, **kwargs):
    from datetime import date
    import logging
    from django.conf import settings
    logger = logging.getLogger(settings.APPLICATION_NAME)
    logger.debug(f"Start database initialization...")
    from django.contrib.auth.models import Group
    from muses_db.models import Member, SiteParam, Section

    logger.debug(f"Members initialization...")
    if not Member.objects.filter(username="muses_admin").exists():
        logger.debug(f"Create superuser muses_admin")
        new_admin = Member.objects.create_superuser(
            username="muses_admin",
            email="user@mail.com",
            date_of_birth=date(2000, 3, 8),
            password="muses_password"
        )

    logger.debug(f"Site parameters initialization...")
    if not SiteParam.objects.filter(key="site_title").exists():
        SiteParam.objects.create_site_param(key="site_title", value="La Passion des Poèmes", protected=True)

    logger.debug(f"Groups initialization...")
    if not Group.objects.filter(name="muses_superusers").exists():
        muses_superusers = Group(name="muses_superusers")
        muses_superusers.save()

    if not Group.objects.filter(name="muses_administrators").exists():
        muses_administrators = Group(name="muses_administrators")
        muses_administrators.save()

    if not Group.objects.filter(name="muses_moderators").exists():
        muses_moderators = Group(name="muses_moderators")
        muses_moderators.save()

    if not Group.objects.filter(name="muses_correctors").exists():
        muses_correctors = Group(name="muses_correctors")
        muses_correctors.save()

    if not Group.objects.filter(name="muses_privileges").exists():
        muses_privileges = Group(name="muses_privileges")
        muses_privileges.save()

    if not Group.objects.filter(name="muses_regulars").exists():
        muses_regulars = Group(name="muses_regulars")
        muses_regulars.save()

    logger.debug(f"Sections initialization...")
    if not Section.objects.filter(short_name="texts").exists():
        section_texts = Section(
            short_name="texts",
            name="Textes",
            description="Section dédiée aux textes",
            order=1,
            status=Section.ACTIVE
        )
        section_texts.save()

    if not Section.objects.filter(short_name="poems").exists():
        section_poems = Section(
            short_name="poems",
            name="Poèmes",
            description="Section dédiée aux poèmes",
            order=2,
            status=Section.ACTIVE
        )
        section_poems.save()

    if not Section.objects.filter(short_name="events").exists():
        section_events = Section(
            short_name="events",
            name="Événementiels",
            description="Section dédiée aux événementiels",
            order=3,
            status=Section.ACTIVE
        )
        section_events.save()

    if not Section.objects.filter(short_name="literarygames").exists():
        section_literarygames = Section(
            short_name="literarygames",
            name="Jeux Littéraires",
            description="Section dédiée aux jeux littéraires",
            order=4,
            status=Section.ACTIVE
        )
        section_literarygames.save()

    if not Section.objects.filter(short_name="discussions").exists():
        section_discussions = Section(
            short_name="discussions",
            name="Discussions",
            description="Section dédiée aux discussions",
            order=5,
            status=Section.ACTIVE
        )
        section_discussions.save()

    if not Section.objects.filter(short_name="news").exists():
        section_news = Section(
            short_name="news",
            name="Actualités",
            description="Section dédiée aux actualités",
            order=6,
            status=Section.ACTIVE
        )
        section_news.save()


class MusesDbConfig(AppConfig):
    name = 'muses_db'

    def ready(self):
        post_migrate.connect(create_required_objects, sender=self)
