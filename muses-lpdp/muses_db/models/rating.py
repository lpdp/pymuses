from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models
from django.utils.translation import gettext_lazy as _

from . import *


class RatingQuerySet(models.QuerySet):
    def post_ratings(self):
        return self.filter(post__isnull=False)

    def comment_ratings(self):
        return self.filter(comment__isnull=False)


class RatingManager(models.Manager):
    def get_queryset(self):
        return RatingQuerySet(self.model, using=self.db)

    def post_ratings(self):
        return self.get_queryset().post_ratings()

    def comment_ratings(self):
        return self.get_queryset().comment_ratings()


class Rating(BaseModel):
    level = models.PositiveSmallIntegerField(
        verbose_name=_("level"),
        unique=False,
        null=False,
        blank=False,
        default=0,
        validators=[MaxValueValidator(5), MinValueValidator(1)],
    )
    owner = models.ForeignKey(
        verbose_name=_("owner"),
        to=Member,
        on_delete=models.CASCADE,
        null=False,
        blank=False,
    )
    post = models.ForeignKey(
        to=Post,
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
    )
    comment = models.ForeignKey(
        to=Comment,
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
    )

    class Meta:
        verbose_name = _("rating")
        verbose_name_plural = _("ratings")
