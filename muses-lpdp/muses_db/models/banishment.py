from django.db import models
from django.utils.translation import gettext_lazy as _

from . import BaseModel, Section


class BanishmentManager(models.Manager):
    pass


class Banishment(BaseModel):
    email = models.EmailField(
        verbose_name=_("email"), unique=True, null=False, blank=False, db_index=True
    )

    details = models.CharField(
        verbose_name=_("details"),
        unique=False,
        null=False,
        blank=True,
        db_index=False,
        max_length=255,
    )
    banner = models.CharField(
        verbose_name=_("banner"),
        unique=False,
        null=False,
        blank=False,
        db_index=False,
        max_length=255,
    )
    mature = models.BooleanField(
        verbose_name=_("mature"), unique=False, null=False, blank=True, default=False
    )
    active = models.BooleanField(
        verbose_name=_("active"), unique=False, null=False, blank=False, default=False
    )

    valid_sections = models.ManyToManyField(to=Section)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _("tag")
        verbose_name_plural = _("tags")
