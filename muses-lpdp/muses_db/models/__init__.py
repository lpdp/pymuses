from .base_model import *
from .section import *
from .site_param import *
from .member import *
from .message import *
from .faq import *
from .tag import *
from .license import *
from .book import *
from .bookpart import *
from .post import *
from .comment import *
from .admonition import *
from .admonition_action import *
from .alert import *
from .correction_request import *
from .correction_request_action import *
from .contest import *
from .contest_round import *
from .voting import *
from .vote import *
from .rating import *
from .spam_domain import *
import datetime


def get_anonymous_user_instance(user_class):
    from django.conf import settings

    new_user = user_class(
        email="anonymous@mail.com", date_of_birth=datetime.date(1970, 1, 1)
    )
    setattr(
        new_user,
        user_class.USERNAME_FIELD,
        getattr(settings, "ANONYMOUS_USER_NAME", "AnonymousUser"),
    )
    return new_user


__all__ = [
    "BaseModel",
    "Section",
    "SiteParam",
    "Member",
    "Message",
    "Faq",
    "Tag",
    "License",
    "Book",
    "BookPart",
    "Post",
    "Comment",
    "Admonition",
    "AdmonitionAction",
    "Alert",
    "CorrectionRequest",
    "CorrectionRequestAction",
    "Contest",
    "ContestRound",
    "Voting",
    "Vote",
    "SpamDomain",
    "Rating",
]
