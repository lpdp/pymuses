from django.db import models
from django.utils.translation import gettext_lazy as _
from . import BaseModel, Post, Member


class VoteManager(models.Manager):
    pass


class Vote(BaseModel):
    member = models.ForeignKey(
        to=Member,
        on_delete=models.CASCADE
    )

    post = models.ForeignKey(
        to=Post,
        on_delete=models.CASCADE
    )

    def __str__(self):
        return self.id

    class Meta:
        verbose_name = _('vote')
        verbose_name_plural = _('votes')