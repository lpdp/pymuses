from django.db import models
from django.utils.translation import gettext_lazy as _

from . import BaseModel


class ListQuerySet(models.QuerySet):
    def favourites_lists(self):
        return self.filter(type=List.FAVOURITES_LIST)

    def reading_lists(self):
        return self.filter(type=List.READING_LIST)


class ListManager(models.Manager):
    def get_queryset(self):
        return ListQuerySet(self.model, using=self._db)

    def favourites_lists(self):
        return self.get_queryset().favourites_lists()

    def reading_lists(self):
        return self.get_queryset().reading_lists()


class List(BaseModel):
    name: str = models.CharField(verbose_name=_("name"), max_length=255, null=True)
    text = models.TextField(verbose_name=_("text"), null=True)
    FAVOURITES_LIST = "favourites list"
    READING_LIST = "reading list"
    TEAM_SELECTION_LIST = "team_selection_list"
    LIST_TYPES_CHOICES = [
        (FAVOURITES_LIST, _("FAVOURITES LIST")),
        (READING_LIST, _("READING LIST")),
        (TEAM_SELECTION_LIST, _("TEAM SELECTION LIST")),
    ]
    type = models.CharField(
        verbose_name=_("type"),
        max_length=30,
        choices=LIST_TYPES_CHOICES,
        default=READING_LIST,
    )
    active = models.BooleanField(
        verbose_name=_("active"), unique=False, null=False, blank=False, default=False
    )

    objects = ListManager()

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _("list")
        verbose_name_plural = _("lists")
