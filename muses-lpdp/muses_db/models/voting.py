from django.db import models
from django.utils.translation import gettext_lazy as _
from . import ContestRound, BaseModel


class VotingManager(models.Manager):
    pass


class Voting(BaseModel):
    name = models.CharField(
        max_length=255, unique=True, null=False, blank=False, db_index=True
    )
    enable_at = models.DateTimeField(
        verbose_name=_("enable at"), unique=False, null=True, blank=True, db_index=True
    )
    disable_at = models.DateTimeField(
        verbose_name=_("disable at"), unique=False, null=True, blank=True, db_index=True
    )
    contest = models.ForeignKey(to=ContestRound, on_delete=models.CASCADE)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _("voting")
        verbose_name_plural = _("votings")
