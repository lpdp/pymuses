from __future__ import annotations

from django.db import models
from django.utils.translation import gettext_lazy as _
from precise_bbcode.fields import BBCodeTextField

from django_edtjs.fields import EditorJSField
from muses_db.models import BaseModel, Member, BookPart, Tag, Section


class PostQuerySet(models.QuerySet):
    def drafts(self):
        return self.filter(status=Post.DRAFT)

    def published(self):
        return self.filter(status=Post.PUBLISHED)

    def warned(self):
        return self.filter(status=Post.WARNED)

    def archived(self):
        return self.filter(status=Post.ARCHIVED)


class PostManager(models.Manager):
    def get_queryset(self):
        return PostQuerySet(self.model, using=self._db)

    def drafts(self):
        return self.get_queryset().drafts()

    def published(self):
        return self.get_queryset().published()

    def warned(self):
        return self.get_queryset().warned()

    def archived(self):
        return self.get_queryset().archived()

    def title_count(self, keyword: str) -> int:
        return self.filter(title__icontains=keyword).count()

    def summary_count(self, keyword: str) -> int:
        return self.filter(summary__icontains=keyword).count()

    def content_count(self, keyword: str) -> int:
        return self.filter(content__icontains=keyword).count()


class Post(BaseModel):
    old_slug = models.URLField(
        verbose_name=_("old slug"),
        max_length=2048,
    )
    DRAFT = "draft"
    PUBLISHED = "published"
    WARNED = "warned"
    ARCHIVED = "archived"
    POST_STATUS_CHOICES = [
        (DRAFT, _("DRAFT")),
        (PUBLISHED, _("PUBLISHED")),
        (WARNED, _("WARNED")),
        (ARCHIVED, _("ARCHIVED")),
    ]
    title = models.CharField(
        verbose_name=_("title"),
        unique=False,
        null=False,
        blank=False,
        max_length=255,
    )
    summary = EditorJSField(
        verbose_name=_("summary"),
        unique=False,
        null=True,
        blank=True,
        max_length=2000,
    )
    bbcode_content = BBCodeTextField(
        verbose_name=_("bbcode content"),
        unique=False,
        null=True,
        blank=True,
    )
    content = EditorJSField(
        verbose_name=_("content"),
        unique=False,
        null=True,
        blank=True,
    )
    status = models.CharField(
        verbose_name=_("status"),
        choices=POST_STATUS_CHOICES,
        max_length=20,
        default=DRAFT,
    )
    hits_counter = models.IntegerField(verbose_name=_("hits count"), default=0)
    revisions_counter = models.IntegerField(
        verbose_name=_("revisions count"), default=0
    )
    validated_at = models.DateTimeField(
        verbose_name=_("validated at"), unique=False, null=True, blank=False
    )
    validated_by = models.ForeignKey(
        to=Member,
        on_delete=models.DO_NOTHING,
        related_name="validated_by",
        null=True,
    )
    readings = models.ManyToManyField(to=Member, related_name="post_readings")
    book_part = models.ForeignKey(
        to=BookPart,
        related_name="book_part_ref",
        on_delete=models.CASCADE,
        null=True,
    )
    tags = models.ManyToManyField(to=Tag)
    revision = models.ForeignKey(
        to="self", related_name="revision_ref", on_delete=models.CASCADE, null=True
    )
    authors = models.ManyToManyField(to=Member, related_name="post_authors")
    section = models.ForeignKey(
        to=Section, related_name="section_ref", on_delete=models.CASCADE
    )
    ip_address = models.GenericIPAddressField(
        verbose_name=_("ip address"), null=True, protocol="both", unpack_ipv4=True
    )

    objects = PostManager()

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = _("post")
        verbose_name_plural = _("posts")
