from django.db import models
from django.utils.translation import gettext_lazy as _

from . import BaseModel, Section


class FaqManager(models.Manager):
    pass


class Faq(BaseModel):
    ask = models.CharField(
        verbose_name=_("Ask"),
        max_length=2048,
        unique=True,
        null=False,
        blank=False,
        db_index=True
    )
    answer = models.TextField(
        verbose_name=_("Answer"),
        null=False,
        blank=False,
    )

    objects = FaqManager()

    def __str__(self):
        return self.ask

    class Meta:
        verbose_name = _('faq')
        verbose_name_plural = _('faqs')
