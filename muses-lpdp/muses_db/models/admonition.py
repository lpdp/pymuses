from __future__ import annotations

from django.db import models
from django.utils.translation import gettext_lazy as _

from . import BaseModel, Post, Comment


class AdmonitionManager(models.Manager):
    pass


class Admonition(BaseModel):
    subject = models.CharField(
        verbose_name=_("subject"),
        unique=False,
        null=False,
        blank=False,
        db_index=True,
        max_length=255,
        default=_("no subject"),
        help_text=_("Define subject of admonition"),
    )
    content = models.TextField(
        verbose_name=_("content"),
        unique=False,
        null=True,
        blank=True,
        help_text=_("Define content of admonition"),
    )
    post = models.ForeignKey(
        to=Post,
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
        help_text=_("If defined, link the related post"),
    )
    comment = models.ForeignKey(
        to=Comment,
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
        help_text=_("If defined, link the related comment"),
    )

    objects = AdmonitionManager()

    def __str__(self):
        return self.subject

    class Meta:
        verbose_name = "admonition"
        verbose_name_plural = "admonitions"
