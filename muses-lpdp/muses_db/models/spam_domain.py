from django.db import models
from django.utils.translation import gettext_lazy as _

from . import BaseModel


class SpamDomainManager(models.Manager):
    pass


class SpamDomain(BaseModel):
    ACTIVE = 1
    DISABLED = 2
    STATUS_CHOICES = ((ACTIVE, _("active")), (DISABLED, _("disabled")))
    name = models.CharField(
        verbose_name=_("domain name"),
        unique=True,
        max_length=500,
        default=_("domain.lpdp"),
        db_index=True,
    )
    status = models.IntegerField(
        verbose_name=_("status"), choices=STATUS_CHOICES, default=DISABLED
    )

    objects = SpamDomainManager()

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _("spam domain")
        verbose_name_plural = _("spam domains")
