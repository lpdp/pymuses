from django.db import models
from django.utils.translation import gettext_lazy as _

from . import BaseModel
from .judging_panel import JudgingPanel
from ..validators import NotPassedValidator


class ContestManager(models.Manager):
    pass


class Contest(BaseModel):
    name = models.CharField(
        verbose_name=_("name"),
        unique=False,
        null=False,
        blank=False,
        max_length=255,
        db_index=True,
    )
    starts_at = models.DateField(
        verbose_name=_("start at"),
        unique=False,
        null=False,
        blank=False,
        db_index=True,
        validators=[NotPassedValidator],
    )
    ends_at = models.DateField(
        verbose_name=_("ends at"),
        unique=False,
        null=False,
        blank=False,
        db_index=True,
        validators=[NotPassedValidator],
    )
    judging_panel = models.ForeignKey(
        to=JudgingPanel,
        related_name="judging_panel_ref",
        on_delete=models.CASCADE,
        null=True,
    )

    objects = ContestManager()

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "contest"
        verbose_name_plural = "contests"
