from django.db import models

from . import BaseModel, Contest


class ContestRoundManager(models.Manager):
    pass


class ContestRound(BaseModel):
    contest = models.ForeignKey(
        to=Contest, related_name="contest_ref", on_delete=models.CASCADE, null=False
    )
