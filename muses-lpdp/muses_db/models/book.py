from django.db import models
from django.utils.translation import gettext_lazy as _

from django_edtjs.fields import EditorJSField
from . import BaseModel, License, Member


class BookManager(models.Manager):
    pass


class Book(BaseModel):
    title = models.CharField(
        verbose_name=_('title'),
        max_length=255,
        help_text=_('Book title'),
    )
    isbn = models.CharField(
        verbose_name=_('isbn'),
        max_length=20,
        help_text=_('Book ISBN')
    )
    summary = EditorJSField(
        verbose_name=_('summary'),
        unique=False,
        null=True,
        blank=True,
        max_length=2000,
        help_text=_('Book summary')
    )
    published_at = models.DateField(
        verbose_name=_('published date'),
        help_text=_('Book published at')
    )
    author = models.ForeignKey(
        to=Member,
        related_name='book_author',
        on_delete=models.CASCADE
    )
    license = models.ForeignKey(
        to=License,
        related_name='book_license',
        on_delete=models.CASCADE,
    )
    visible = models.BooleanField(
        verbose_name=_('visible'),
        unique=False,
        null=False,
        blank=False,
        default=False,
        help_text=_('Specify the status of book'),
    )

    objects = BookManager()

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = _('book')
        verbose_name_plural = _('books')
