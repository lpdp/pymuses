from django.db import models
from django.utils.translation import gettext_lazy as _

from . import BaseModel, Section


class TagQuerySet(models.QuerySet):
    def categories(self):
        return self.filter(type=Tag.CATEGORY)

    def themes(self):
        return self.filter(type=Tag.THEME)

    def prosodies(self):
        return self.filter(type=Tag.PROSODY)

    def forms(self):
        return self.filter(type=Tag.FORM)

    def users(self):
        return self.filter(type=Tag.USER)

    def events(self):
        return self.filter(type=Tag.EVENT)


class TagManager(models.Manager):
    def get_queryset(self):
        return TagQuerySet(self.model, using=self._db)

    def categories(self):
        return self.get_queryset().categories()

    def themes(self):
        return self.get_queryset().themes()

    def prosodies(self):
        return self.get_queryset().prosodies()

    def forms(self):
        return self.get_queryset().forms()

    def users(self):
        return self.get_queryset().users()

    def events(self):
        return self.get_queryset().events()


class Tag(BaseModel):
    name = models.CharField(
        max_length=255,
        unique=True,
        null=False,
        blank=False,
        db_index=True
    )
    enable_at = models.DateTimeField(
        verbose_name=_('enable at'),
        unique=False,
        null=True,
        blank=True,
        db_index=True
    )
    disable_at = models.DateTimeField(
        verbose_name=_('disable at'),
        unique=False,
        null=True,
        blank=True,
        db_index=True
    )
    CATEGORY = 0
    THEME = 1
    PROSODY = 2
    FORM = 3
    USER = 4
    EVENT = 5
    TYPE_CHOICES = (
        (CATEGORY, _('category')),
        (THEME, _('theme')),
        (PROSODY, _('prosody')),
        (FORM, _('form')),
        (USER, _('user')),
        (EVENT, _('event')),
    )
    type = models.CharField(
        verbose_name=_('type'),
        unique=False,
        null=False,
        blank=True,
        db_index=False,
        max_length=255,
        choices=TYPE_CHOICES,
        default=USER
    )
    mature = models.BooleanField(
        verbose_name=_('mature'),
        unique=False,
        null=False,
        blank=True,
        default=False
    )
    active = models.BooleanField(
        verbose_name=_('active'),
        unique=False,
        null=False,
        blank=False,
        default=False
    )

    valid_sections = models.ManyToManyField(
        to=Section
    )

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _('tag')
        verbose_name_plural = _('tags')
