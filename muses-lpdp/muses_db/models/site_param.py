from django.db import models
from django.utils.translation import gettext_lazy as _

from . import BaseModel


class SiteParamManager(models.Manager):
    def create_site_param(self, key: str, value: str, protected=False):
        return SiteParam.create_if_not_exists(key=key, value=value, protected=protected)


class SiteParam(BaseModel):
    key = models.CharField(
        verbose_name=_('key'),
        unique=True,
        null=False,
        blank=False,
        db_index=True,
        max_length=255,
        default=_("no name")
    )
    value = models.CharField(
        verbose_name=_("value"),
        unique=True,
        null=False,
        blank=False,
        default=_('no value'),
        db_index=True,
        max_length=2048
    )
    protected = models.BooleanField(
        verbose_name=_('protected'),
        null=False,
        blank=False,
        default=False
    )

    objects = SiteParamManager()

    class Meta:
        verbose_name = _('site parameter')
        verbose_name_plural = _('site parameters')

    @classmethod
    def create_if_not_exists(cls, key: str, value: str, protected=False):
        if not cls.objects.filter(key=key).exists():
            site_param = cls(key=key, value=value, protected=protected)
            site_param.save()
        else:
            site_param = cls.objects.filter(key=key)[0]
        return site_param
