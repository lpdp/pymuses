from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models
from django.utils.translation import gettext_lazy as _

from . import BaseModel, Member, Post, Comment


class LikeQuerySet(models.QuerySet):
    def post_likes(self):
        return self.filter(post__isnull=False)

    def comment_likes(self):
        return self.filter(comment__isnull=False)


class LikeManager(models.Manager):
    def get_queryset(self):
        return LikeQuerySet(self.model, using=self.db)

    def post_likes(self):
        return self.get_queryset().post_likes()

    def comment_likes(self):
        return self.get_queryset().comment_likes()


class Like(BaseModel):
    level = models.PositiveSmallIntegerField(
        verbose_name=_('level'),
        unique=False,
        null=False,
        blank=False,
        default=0,
        validators=[
            MaxValueValidator(5),
            MinValueValidator(1)
        ],
    )
    owner = models.ForeignKey(
        verbose_name=_('owner'),
        to=Member,
        on_delete=models.CASCADE,
        null=False,
        blank=False,
    )
    post = models.ForeignKey(
        to=Post,
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
    )
    comment = models.ForeignKey(
        to=Comment,
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
    )

    class Meta:
        verbose_name = _('like')
        verbose_name_plural = _('likes')
