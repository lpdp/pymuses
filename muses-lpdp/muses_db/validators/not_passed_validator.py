from django.utils.deconstruct import deconstructible
from django.utils.translation import ugettext_lazy as _
from django.core.validators import BaseValidator
from datetime import datetime


def not_passed(value):
    return not ((value - datetime.now()).total_seconds() > 0)


@deconstructible
class NotPassedValidator(BaseValidator):
    message = _("%(value)s is passed")
    code = "not_passed"

    def __call__(self, value):
        return not_passed(value)
