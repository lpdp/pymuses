from django.apps import AppConfig


class DjangoTagsSelectInputConfig(AppConfig):
    name = 'django_tags_select_input'
