"""muses_core URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django import get_version
from django.contrib import admin
from django.shortcuts import redirect
from django.urls import path, include
from django.conf.urls.i18n import i18n_patterns
from django.views.decorators.cache import cache_page
from django.views.decorators.csrf import csrf_exempt
from django.views.i18n import JavaScriptCatalog
from graphene_django.views import GraphQLView

urlpatterns = i18n_patterns(
    path(
        "jsi18n/",
        cache_page(86400, key_prefix="js18n-%s" % get_version())(
            JavaScriptCatalog.as_view()
        ),
        name="javascript-catalog",
    ),
    path("admin/", admin.site.urls),
    path("web/", include("muses_web.urls")),
    prefix_default_language=True,
)

urlpatterns += [
    path("i18n/", include("django.conf.urls.i18n")),
    path(
        "graphql",
        csrf_exempt(GraphQLView.as_view(graphiql=True)),
        name="muses_graphiql",
    ),
    path("rest/", include("muses_api.urls")),
    path("", lambda request: redirect("home", permanent=True)),
]
