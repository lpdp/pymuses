"""
Django settings for muses_core project.

Generated by 'django-admin startproject' using Django 3.1.1.

For more information on this file, see
https://docs.djangoproject.com/en/3.1/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/3.1/ref/settings/
"""

from os import getenv, path
from pathlib import Path
from django.utils.translation import gettext_lazy as _

# Build paths inside the project like this: BASE_DIR / 'subdir'.
from config_box import ConfigBox

BASE_DIR = Path(__file__).resolve().parent.parent

ENVIRONMENT = getenv("MUSES_ENVIRONMENT", "LOCAL")

CONFIG_PATH = getenv(
    "MUSES_CONFIG_PATH",
    path.join(
        Path(BASE_DIR).resolve(strict=True).parent, "localsettings", "config.yml"
    ),
)
SECRET_PATH = getenv(
    "MUSES_SECRET_PATH",
    path.join(
        Path(BASE_DIR).resolve(strict=True).parent, "localsettings", "secrets.yml"
    ),
)

CONFIG = ConfigBox(
    CONFIG_PATH,
    SECRET_PATH,
)

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/3.1/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = (
    "s%tf5a8)swy8pw&35&ou-yaqhjw@ty(76htqu4o=h1q&yafct%"
    if ENVIRONMENT not in ["DEV", "STG", "PRD"]
    else CONFIG.get("secret_key")
)

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True if ENVIRONMENT not in ["PRD"] else False

ALLOWED_HOSTS = [] if not CONFIG.get("allowed_hosts") else CONFIG.get("allowed_hosts")

# Application definition

INSTALLED_APPS = [
    "django_edtjs.apps.DjangoEdtjsConfig",
    "django_tags_select_input.apps.DjangoTagsSelectInputConfig",
    "muses_bbcode.apps.MusesBbcodeConfig",
    "muses_db.apps.MusesDbConfig",
    "muses_api.apps.MusesApiConfig",
    "django.contrib.admin",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",
    "guardian",
    "graphene_django",
    "rest_framework",
    "rest_framework.authtoken",
    "muses_web.apps.MusesWebConfig",
]

MIDDLEWARE = [
    "django.middleware.security.SecurityMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.locale.LocaleMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
    "muses_web.middlewares.Theme"
    # 'django_user_pages_views_mdw.apps.DjangoUserPagesViewsMdwConfig',
]

ROOT_URLCONF = "muses_core.urls"

LOGIN_URL = "/web/login/"

# Authentication User Model
AUTH_USER_MODEL = "muses_db.Member"

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [],
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
                "django.template.context_processors.i18n",
                "muses_web.context_processors.global_context_processor",
                "muses_web.context_processors.main_menu_context_processor",
            ],
        },
    },
]

WSGI_APPLICATION = "muses_core.wsgi.application"

# Database
# https://docs.djangoproject.com/en/3.1/ref/settings/#databases

DATABASES = (
    {
        "default": {
            "ENGINE": "django.db.backends.sqlite3",
            "NAME": BASE_DIR / "db.sqlite3",
        }
    }
    if ENVIRONMENT not in ["DEV", "STG", "PRD"]
    else {
        "default": {
            "ENGINE": "django.db.backends.postgresql",
            "NAME": "mydatabase",
            "USER": "mydatabaseuser",
            "PASSWORD": "mypassword",
            "HOST": "127.0.0.1",
            "PORT": "5432",
        }
    }
)

AUTHENTICATION_BACKENDS = (
    "django.contrib.auth.backends.ModelBackend",  # this is default
    "guardian.backends.ObjectPermissionBackend",
)

GUARDIAN_GET_INIT_ANONYMOUS_USER = "muses_db.models.get_anonymous_user_instance"

# Password validation
# https://docs.djangoproject.com/en/3.1/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        "NAME": "django.contrib.auth.password_validation.UserAttributeSimilarityValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.MinimumLengthValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.CommonPasswordValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.NumericPasswordValidator",
    },
]

# Internationalization
# https://docs.djangoproject.com/en/3.1/topics/i18n/

LANGUAGE_COOKIE_NAME = "muses-lng"

LANGUAGES = [
    ("fr", _("French")),
    ("en", _("English")),
]

LANGUAGE_CODE = "en" if CONFIG.get("language_code") else CONFIG.get("language_code")

TIME_ZONE = "UTC" if CONFIG.get("time_zone") is None else CONFIG.get("time_zone")

USE_I18N = True

USE_L10N = True

USE_TZ = True

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/3.1/howto/static-files/

STATIC_URL = (
    "http://127.0.0.1:8001/"
    if CONFIG.get("static_url") is None
    else CONFIG.get("static_url")
)
S_BASE_DIR = path.join(Path(BASE_DIR).resolve(strict=True), "static")
STATIC_ROOT = S_BASE_DIR
MEDIA_URL = (
    "http://127.0.0.1:8002/"
    if CONFIG.get("media_url") is None
    else CONFIG.get("media_url")
)
S_MEDIA_ROOT = path.join(Path(BASE_DIR).resolve(strict=True), "media")
MEDIA_ROOT = S_MEDIA_ROOT

EMAIL_BACKEND = "django.core.mail.backends.smtp.EmailBackend"
EMAIL_HOST = getenv(
    "EMAIL_HOST",
    CONFIG.get("email_host") if CONFIG.get("email_host") is not None else "localhost",
)
EMAIL_PORT = getenv(
    "EMAIL_PORT",
    CONFIG.get("email_port") if CONFIG.get("email_port") is not None else "25",
)
EMAIL_HOST_USER = getenv(
    "EMAIL_HOST_USER",
    CONFIG.get("email_host_user") if CONFIG.get("email_host_user") is not None else "",
)
EMAIL_HOST_PASSWORD = getenv(
    "EMAIL_HOST_PASSWORD",
    CONFIG.get("email_host_password")
    if CONFIG.get("email_host_password") is not None
    else "",
)
EMAIL_SUBJECT_PREFIX = getenv(
    "EMAIL_SUBJECT_PREFIX",
    CONFIG.get("email_subject_prefix")
    if CONFIG.get("email_subject_prefix") is not None
    else "[Muses LPDP]",
)
EMAIL_USE_LOCALTIME = getenv(
    "EMAIL_USE_LOCALTIME",
    CONFIG.get("email_use_localtime")
    if CONFIG.get("email_use_localtime") is not None
    else False,
)
EMAIL_USE_TLS = getenv(
    "EMAIL_USE_TLS",
    CONFIG.get("email_use_tls") if CONFIG.get("email_use_tls") is not None else False,
)
EMAIL_USE_SSL = getenv(
    "EMAIL_USE_SSL",
    CONFIG.get("email_use_ssl") if CONFIG.get("email_use_ssl") is not None else False,
)

LOCALE_PATHS = [f"{BASE_DIR}/locale"]

APPLICATION_NAME = "muses"

GRAPHENE = {"SCHEMA": "muses_api.schema.schema"}

REST_FRAMEWORK = {
    # Use Django's standard `django.contrib.auth` permissions,
    # or allow read-only access for unauthenticated users.
    "DEFAULT_PERMISSION_CLASSES": [
        "rest_framework.permissions.DjangoModelPermissionsOrAnonReadOnly"
    ],
    "DEFAULT_PAGINATION_CLASS": "rest_framework.pagination.PageNumberPagination",
    "PAGE_SIZE": 10,
}

LOGGING = {
    "version": 1,
    "disable_existing_loggers": False,
    "formatters": {
        "verbose": {
            "format": "{levelname} {asctime} {name} {module} {process:d} {thread:d} {message}",
            "style": "{",
        },
        "simple": {
            "format": "{levelname} {message}",
            "style": "{",
        },
    },
    "handlers": {
        "console": {"class": "logging.StreamHandler", "formatter": "verbose"},
    },
    "loggers": {
        "django": {
            "handlers": ["console"],
            "propagate": False,
        },
        APPLICATION_NAME: {
            "handlers": ["console"],
            "propagate": False,
        },
    },
    "root": {
        "handlers": ["console"],
        "level": "DEBUG",
    },
}
