from django.apps import AppConfig


class MusesBbcodeConfig(AppConfig):
    name = 'muses_bbcode'
